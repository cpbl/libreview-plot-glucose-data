import pandas as pd
import matplotlib.pyplot as plt
__author__='cpbl'

histcol, scancol = 'Historic Glucose mmol/L','Scan Glucose mmol/L'
infile= ''#'glucose_2021-10-3-Libreview.csv'
if not infile:
    from tkinter import filedialog as fd
    infile = fd.askopenfilename()

df=pd.read_table(infile, sep=',', skiprows=1)
df['glucose'] = df[histcol].fillna(df[scancol])
df['Date'] = pd.to_datetime(df['Device Timestamp'],dayfirst=True)
df=df.set_index('Date')

dfh, dfs = df.dropna(subset=[histcol]), df.dropna(subset=[scancol])

fig,ax = plt.subplots(1, figsize=(15,5))
df.glucose.plot(ax=ax)
plt.ylabel('Interstitial glucose (mmol/L)')
ax.plot(dfs.index, dfs.glucose, 'r.')
plt.savefig('tmp-glucose.pdf')
plt.savefig('tmp-glucose.png')
plt.show()
